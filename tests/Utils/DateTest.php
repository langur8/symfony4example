<?php
/**
 * Created by PhpStorm.
 * User: langur
 * Date: 14.03.2018
 * Time: 19:08
 */

namespace App\Tests\Utils;

use App\Utils\Date;
use PHPUnit\Framework\TestCase;

/**
 * Class DateTest
 * @package App\Tests\Utils
 */
class DateTest extends TestCase
{

    /**
     * @dataProvider getDurations
     */
    public function testDurations(string $duration, \DateTime $from, \DateTime $to)
    {
        $this->assertEquals($duration, Date::duration($from, $to));
    }

    /**
     * @return \Generator
     */
    public function getDurations()
    {
        $now = new \DateTime();
        $yearBefore = clone $now;
        $yearBefore->modify('- 1 year');
        $allUnits = clone $now;
        $allUnits->modify('-5 years');
        $allUnits->modify('-2 months');
        $allUnits->modify('-3 days');
        $allUnits->modify('-10 hours');
        $allUnits->modify('-4 minutes');
        yield['00:00', $now, $now];
        yield['00:00', $now, $yearBefore];
        yield['1Y 00:00', $yearBefore, $now];
        yield['5Y 2M 3D 10:04', $allUnits, $now];
    }


}
