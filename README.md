# README #

Ukázková aplikace Symfony4, Doctrine2, Twig v prostredi Apache2 + PHP 7.1 nebo vyssi

Instalace pomoci Composer, Webpack Encore a Doctrine Migrations:

`$ composer install`

`$ yarn (npm install)`

`$ yarn encore dev`

`$ php bin/console doctrine:migrations:diff`

`$ php bin/console doctrine:migrations:migrate`

Testy:
 
`$ ./vendor/bin/simple-phpunit`
