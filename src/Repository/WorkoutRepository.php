<?php
/**
 * Created by PhpStorm.
 * User: langur
 * Date: 13.03.2018
 * Time: 20:43
 */

namespace App\Repository;


use App\Entity\Workout;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class WorkoutRepository extends ServiceEntityRepository
{
    /**
     * WorkoutRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Workout::class);
    }


}