<?php
/**
 * Created by PhpStorm.
 * User: langur
 * Date: 13.03.2018
 * Time: 20:31
 */

namespace App\Controller;


use App\Entity\Workout;
use App\Form\WorkoutType;
use App\Repository\WorkoutRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WorkoutController used to manage Workouts
 *
 * @package App\Controller
 */
class WorkoutController extends Controller
{

    /**
     * @Route("/")
     * @Method("GET")
     *
     * @param WorkoutRepository $repo
     * @return Response
     */
    public function index(WorkoutRepository $repo): Response
    {
        return $this->render('workouts/index.html.twig', ['workouts' => $repo->findAll()]);
    }

    /**
     * @Route("/add")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $workout = new Workout();
        $form = $this->createForm(WorkoutType::class, $workout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($workout);
            $em->flush();
            $this->addFlash('success', 'workout.created');
            return $this->redirectToRoute('app_workout_index');
        }
        return $this->render('workouts/add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param Workout $workout
     * @return Response
     */
    public function edit(Request $request, Workout $workout): Response
    {
        $form = $this->createForm(WorkoutType::class, $workout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($workout);
            $em->flush();
            $this->addFlash('success', 'workout.edited');
            return $this->redirectToRoute('app_workout_index');
        }
        return $this->render('workouts/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete/{id}")
     * @Method({"GET", "POST"})
     *
     * @param Workout $workout
     * @return Response
     */
    public function delete(Workout $workout): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($workout);
        $em->flush();
        $this->addFlash('success', 'workout.deleted');
        return $this->redirectToRoute('app_workout_index');
    }

}