<?php
/**
 * Created by PhpStorm.
 * User: langur
 * Date: 14.03.2018
 * Time: 15:17
 */

namespace App\Form;

use App\Entity\Post;
use App\Entity\Workout;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class WorkoutType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'attr'    => ['autofocus' => true],
                'label'   => 'label.type',
                'choices' => array_flip(Workout::getTypes())
            ])
            ->add('distance', null, [
                'attr'  => ['autofocus' => true],
                'label' => 'label.distance',
            ])
            ->add('startTime', null, [
                'label' => 'label.startTime',
            ])
            ->add('finishTime', null, [
                'attr'  => ['rows' => 20],
                'label' => 'label.finishTime',
            ])
            ->add('private', null, [
                'label'    => 'label.private',
                'required' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'label.save'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Workout::class,
        ]);
    }
}
