<?php
/**
 * Created by PhpStorm.
 * User: langur
 * Date: 14.03.2018
 * Time: 17:40
 */

namespace App\Utils;


class Date
{
    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return string
     */
    public static function duration(\DateTime $from, \DateTime $to): string
    {
        if($to <= $from) {
            return '00:00';
        }
        $interval = $to->diff($from);
        $format = [];
        if($interval->y > 0) {
            $format[] = '%yY';
        }
        if($interval->m > 0) {
            $format[] = '%mM';
        }
        if($interval->d > 0) {
            $format[] = '%dD';
        }
        $format[] = '%H:%I';
        return $interval->format(implode(' ', $format));
    }
}