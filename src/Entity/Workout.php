<?php
/**
 * Created by PhpStorm.
 * User: langur
 * Date: 13.03.2018
 * Time: 20:31
 */

namespace App\Entity;

use App\Utils\Date;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Workout
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\WorkoutRepository")
 */
class Workout
{
    const TYPE_RUN = 1;
    const TYPE_RIDE = 2;

    private static $types = [
        self::TYPE_RUN  => 'Run',
        self::TYPE_RIDE => 'Ride'
    ];

    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column("string")
     */
    protected $id;

    /**
     * Type of workout
     *
     * @var int
     * @ORM\Column(type="smallint")
     */
    protected $type;

    /**
     * Distance in km
     *
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $distance;

    /**
     * Starting time
     *
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $startTime;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $finishTime;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $private = false;

    /**
     * @return null|string
     */
    public function getTypeString(): ?string
    {
        return isset(static::$types[$this->type]) ? static::$types[$this->type] : null;
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return static::$types;
    }

    /**
     * Return string representation of duration
     * @return null|string
     */
    public function getDuration(): ?string
    {
        if($this->startTime === null || $this->finishTime === null) {
            return null;
        }
        return Date::duration($this->startTime, $this->finishTime);
    }

//GENERATED
    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getDistance(): ?int
    {
        return $this->distance;
    }

    /**
     * @param int $distance
     */
    public function setDistance(int $distance): void
    {
        $this->distance = $distance;
    }

    /**
     * @return \DateTime
     */
    public function getStartTime(): ?\DateTime
    {
        return $this->startTime;
    }

    /**
     * @param \DateTime $startTime
     */
    public function setStartTime(\DateTime $startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * @return \DateTime
     */
    public function getFinishTime(): ?\DateTime
    {
        return $this->finishTime;
    }

    /**
     * @param \DateTime $finishTime
     */
    public function setFinishTime(\DateTime $finishTime): void
    {
        $this->finishTime = $finishTime;
    }

    /**
     * @return bool
     */
    public function isPrivate(): ?bool
    {
        return $this->private;
    }

    /**
     * @param bool $private
     */
    public function setPrivate(bool $private): void
    {
        $this->private = $private;
    }


}